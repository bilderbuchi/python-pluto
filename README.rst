Python-Pluto
============

Parse ECSS PLUTO scripts to Python code.

Getting Started
---------------

Clone the repository and prepare a virtual environment for Python3.

.. code:: bash

  git clone https://gitlab.com/librecube/prototypes/python-pluto
  cd python-pluto
  virtualenv -p python3 venv
  source venv/bin/activate
  pip install -r requirements.txt
  pip install -e .

Examples
--------

The examples folder contains PLUTO procedures and the corresponding generated Python code.

Parsing
~~~~~~~

To parse a PLUTO procedure to Python script, a command line utility gets installed with the package:

.. code:: bash

  pluto_convert some_procedure.pluto

This will create a Python file ``some_procedure.py`` at the same location.

Running
~~~~~~~

To run a converted Python script:

.. code:: bash

  pluto_run some_procedure.py

Testing
~~~~~~~

For testing, run *pytest* on the command line.
