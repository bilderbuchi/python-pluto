from pluto import *
from model import *


class Procedure_procedure_main_body(Procedure):

    def main(self):
        stp = Step_DummyStep(self)
        if self.initiate_and_confirm_step(stp) is False: return False
        act = ActivityCall(self, DummyElement.DummyActivity)
        if self.initiate_and_confirm_activity(act) is False: return False
        act = ActivityCall(self, DummyElement.DummyActivity)
        self.initiate_activity(act)
        self.inform_user(str("inform user statement"))
        self.log(str("log statement"))


class Step_DummyStep(Step):

    def main(self):
        self.log(str("initiate and confirm step statement"))
