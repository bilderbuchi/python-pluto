"""This file defines the Monitoring and Control Model upon which the
Executor acts upon and which are referenced in the PLUTO procedures. Namely,
the Executor reads Python scripts (procedures) generated from the PLUTO parser
and runs them.
"""
import functools

from mcm import SystemElement, Activity, ReportingData
from pluto import ureg

# TODO: load definitions of system model from a YAML file!


# create system elements
DummyElement = SystemElement("DummyElement")
Satellite = SystemElement("Satellite")
AOCS = SystemElement("AOCS")
Gyro3 = SystemElement("Gyro3")


# create system model hierarchy
Satellite.add_child_element(Gyro3)
Satellite.add_child_element(AOCS)


# dummy function to be used for activity calls
def func(name, arguments):
    i = input("Activity [" + name + "] is called. Fail its execution (f)?")
    if i.lower() == 'f':
        return False
    return True


# Activities

act = Activity("DummyActivity")
act.run = lambda x: print("DummyActivity called")
DummyElement.add_activity(act)

act = Activity("ZDW17001")
act.run = functools.partial(func, "ZDW17001")
AOCS.add_activity(act)

act = Activity("SpacecraftStatusOK")
act.run = functools.partial(func, "SpacecraftStatusOK")
Satellite.add_activity(act)

act = Activity("ZOB90140")
act.run = functools.partial(func, "ZOB90140")
act.get_confirmation_status = lambda: "confirmed"
Satellite.add_activity(act)

ActivateGIM = Activity("ActivateGIM")
ActivateGIM.run = functools.partial(func, "ActivateGIM")

ActiveBusAcquisition = Activity("ActiveBusAcquisition")
ActiveBusAcquisition.run = functools.partial(func, "ActiveBusAcquisition")

ExitGroundInterventionMode = Activity("ExitGroundInterventionMode")
ExitGroundInterventionMode.run = functools.partial(func, "ExitGroundInterventionMode")

ActiveCoarseMode = Activity("ActiveCoarseMode")
ActiveCoarseMode.run = functools.partial(func, "ActiveCoarseMode")

SwitchBusFromBtoA = Activity("SwitchBusFromBtoA")
SwitchBusFromBtoA.run = functools.partial(func, "SwitchBusFromBtoA")

DeactivateGIM = Activity("DeactivateGIM")
DeactivateGIM.run = functools.partial(func, "DeactivateGIM")


# Reporting data

rep = ReportingData("Temperature")
rep.get_value = lambda: ureg('65 degC')
Gyro3.add_reporting_data(rep)

AOCS_Mode = ReportingData("Temperature")
AOCS_Mode.get_value = lambda: "GIM"
Gyro3.add_reporting_data(rep)

Pitch = ReportingData("Temperature")
Pitch.get_value = lambda: ureg('5 deg')

Roll = ReportingData("Temperature")
Roll.get_value = lambda: ureg('15 deg')

Yaw = ReportingData("Temperature")
Yaw.get_value = lambda: ureg('5 deg')
