from pluto import *
from model import *


class Procedure_log_statement(Procedure):

    def main(self):
        self.log(str("Hello \"my\" World, No.") + str(ureg('42')) + str("!"))
        self.log(str("Hello ") + str("World") + str("!"))
        self.log(str(ureg('1')) + str(ureg('1')+ureg('1')) + str(ureg('6')/ureg('2')))
        self.log(str("The value of 2 * 4 = ") + str(ureg('2')*ureg('4')) + str(", right?"))
        self.log(str("Is 5 > 2.5? ") + str(ureg('5')>ureg('2.5')) + str(". Is 6 < 3? ") + str(ureg('6')<ureg('3')))
        stp = Step_LogStep(self)
        if self.initiate_and_confirm_step(stp) is False: return False


class Step_LogStep(Step):

    def main(self):
        self.log(str("Hello from a Step!"))
