from pluto import *
from model import *


class Procedure_wait_statement(Procedure):

    def preconditions(self):
        self.wait_until_absolute_time(datetime(2001, 8, 18, 21, 7, 43, 137468))
        self.wait_for_relative_time(ureg('1s'))
        if self.wait_until_expression(lambda: ureg('10')>ureg('5')) is False: return False

    def main(self):
        self.log(str("main body"))
        stp = Step_WaitStep(self)
        if self.initiate_and_confirm_step(stp) is False: return False

    def confirmation(self):
        self.wait_until_absolute_time(datetime(2001, 8, 18, 21, 7, 43, 137468))
        self.wait_for_relative_time(ureg('1s'))
        if self.wait_until_expression(lambda: ureg('10')>ureg('5')) is False: return False


class Step_WaitStep(Step):

    def preconditions(self):
        self.wait_until_absolute_time(datetime(2001, 8, 18, 21, 7, 43, 137468))
        self.wait_for_relative_time(ureg('1s'))
        if self.wait_until_expression(lambda: ureg('10')>ureg('5')) is False: return False

    def main(self):
        self.log(str("step body"))
        self.wait_until_absolute_time(datetime(2001, 8, 18, 21, 7, 43, 137468))
        self.wait_for_relative_time(ureg('1s'))
        if self.wait_until_expression(lambda: ureg('10')>ureg('5')) is False: return False

    def confirmation(self):
        self.wait_until_absolute_time(datetime(2001, 8, 18, 21, 7, 43, 137468))
        self.wait_for_relative_time(ureg('1s'))
        if self.wait_until_expression(lambda: ureg('10')>ureg('5')) is False: return False
