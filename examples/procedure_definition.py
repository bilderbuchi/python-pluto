from pluto import *
from model import *


class Procedure_procedure_definition(Procedure):

    def declare(self):
        evt = Event_DummyEvent1(self)
        self.event['DummyEvent1'] = evt

    def preconditions(self):
        self.wait_for_relative_time(ureg('1s'))
        self.wait_for_relative_time(ureg('1s'))

    def main(self):
        self.log(str("main body"))

    def register_watchdogs(self):
        wdg = Watchdog_RecoverDummyEvent1(self)
        self.watchdog['RecoverDummyEvent1'] = wdg

    def confirmation(self):
        if self.wait_until_expression(lambda: True) is False: return False


class Event_DummyEvent1(Event):

    pass


class Watchdog_RecoverDummyEvent1(Watchdog):

    def preconditions(self):
        if self.wait_for_event('DummyEvent1') is False: return False

    def main(self):
        self.log(str("watchdog: RecoverDummyEvent1"))
