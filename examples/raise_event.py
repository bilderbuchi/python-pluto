from pluto import *
from model import *


class Procedure_raise_event(Procedure):

    def declare(self):
        evt = Event_TimeoutEvent(self)
        self.event['TimeoutEvent'] = evt

    def main(self):
        self.log(str("main body"))
        stp = Step_DummyStep(self)
        if self.initiate_and_confirm_step(stp) is False: return False
        self.log(str("end of main body"))

    def register_watchdogs(self):
        wdg = Watchdog_RecoverTimeout(self)
        self.watchdog['RecoverTimeout'] = wdg


class Event_TimeoutEvent(Event):

    pass


class Step_DummyStep(Step):

    def main(self):
        self.log(str("step body"))
        if self.wait_until_expression(lambda: ureg('5')>ureg('10'), timeout=ureg('1s'), raise_event='TimeoutEvent') is False: return False


class Watchdog_RecoverTimeout(Watchdog):

    def preconditions(self):
        if self.wait_for_event('TimeoutEvent') is False: return False

    def main(self):
        self.log(str("watchdog body"))
