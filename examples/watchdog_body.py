
from pluto import *
from model import *


class Procedure_watchdog_body(Procedure):

    def register_watchdogs(self):
        wdg = Watchdog_CheckDepointing(self)
        self.watchdog['CheckDepointing'] = wdg

    def main(self):
        stp = Step_EnterGroundInterventionMode(self)
        if self.initiate_and_confirm_step(stp) is False: return False

        stp = Step_ReconfigureDataBus(self)
        if self.initiate_and_confirm_step(stp) is False: return False

        stp = Step_ExitGroundInterventionMode(self)
        if self.initiate_and_confirm_step(stp) is False: return False


class Watchdog_CheckDepointing(Watchdog):

    def preconditions(self):
        if self.wait_until_expression(lambda: Pitch.get_value() > ureg('10 deg') or Roll.get_value() > ureg('10 deg') or Yaw.get_value() > ureg('10 deg')) is False: return False

    def main(self):
        act = ActivityCall(self, ActiveBusAcquisition)
        if self.initiate_and_confirm_activity(act) is False: return False
        act = ActivityCall(self, ExitGroundInterventionMode)
        if self.initiate_and_confirm_activity(act) is False: return False
        act = ActivityCall(self, ActiveCoarseMode)
        if self.initiate_and_confirm_activity(act) is False: return False


class Step_EnterGroundInterventionMode(Step):

    def main(self):
        act = ActivityCall(self, ActivateGIM)
        if self.initiate_and_confirm_activity(act) is False: return False


class Step_ReconfigureDataBus(Step):

    def preconditions(self):
        if self.wait_until_expression(lambda: AOCS_Mode.get_value() == "GIM") is False: return False

    def main(self):
        act = ActivityCall(self, SwitchBusFromBtoA)
        if self.initiate_and_confirm_activity(act) is False: return False
        act = ActivityCall(self, ActiveBusAcquisition)
        if self.initiate_and_confirm_activity(act) is False: return False


class Step_ExitGroundInterventionMode(Step):

    def main(self):
        act = ActivityCall(self, DeactivateGIM)
        if self.initiate_and_confirm_activity(act) is False: return False
