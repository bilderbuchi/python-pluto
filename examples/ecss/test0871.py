from pluto import *


class Procedure_test0871(Procedure):

    def main(self):
        arguments = [
            {
                "Telecommand": Activity(self, "SwitchOnTelescope"),
                "ReleaseTime": Argument(value="2004-117T12:15:00.0Z"),
                "Subschedule": Argument(value=1),
                "InterlockSet": Argument(value=1)
            },
            {
                "Telecommand": Activity(self, "SwitchOnTelescopeHT"),
                "ReleaseTime": Argument(value="2004-117T12:17:00.0Z"),
                "Subschedule": Argument(value=1),
                "InterlockSet": Argument(value=2),
                "InterlockAssessed": Argument(value=1)
            },
            {
                "Telecommand": Activity(self, "SetTelescopeHT", arguments=[{"Level": ureg("1000V")}]),
                "ReleaseTime": Argument(value="2004-117T12:19:00.0Z"),
                "Subschedule": Argument(value=1),
                "InterlockAssessed": Argument(value=1)
            },
        ]
        act = Activity(self, "InsertIntoSchedule", arguments=arguments)
        if self.initiate_and_confirm_activity(act) is False:
            return False
