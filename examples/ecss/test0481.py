from pluto import *


class Procedure_test0481(Procedure):

	def declare(self):
		wtg = ManoeuvreDampingFailed("Cross-track error not reducing to less than 5 arcmin within 100 s of manoeuvre completion")
		self.watchdog['ManoeuvreDampingFailed'] = wtg

	def main(self):
		self.log("...")


class ManoeuvreDampingFailed(Watchdog):

    def main(self):
        self.log("...")
