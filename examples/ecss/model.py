"""This file defines the Monitoring and Control Model upon which the
Executor acts upon and which are referenced in the PLUTO procedures. Namely,
the Executor reads Python scripts (procedures) generated from the PLUTO parser
and runs them.
"""
import functools

from ssm import SpaceSystemModel, SystemElement, Activity, ReportingData
from pluto import ureg


spacecraft = SystemElement(name="Satellite")
sysmodel = SpaceSystemModel()
sysmodel.add_root_element(spacecraft)


def func(name, arguments):
    i = input("Activity [" + name + "] is called. Fail its execution (f)?")
    if i.lower() == 'f':
        return False
    return True


act = Activity("My Activity", spacecraft)
act.function = functools.partial(func, "My Activity")
spacecraft.add_activity(act)

act = Activity("ZOB90140", spacecraft)
act.function = functools.partial(func, "ZOB90140")
spacecraft.add_activity(act)

act = Activity("SwitchOnGyroConverter", spacecraft)
act.function = functools.partial(func, "SwitchOnGyroConverter")
spacecraft.add_activity(act)

act = Activity("SwitchOnGyro5", spacecraft)
act.function = functools.partial(func, "SwitchOnGyro5")
spacecraft.add_activity(act)

act = Activity("Gyro5FineMode", spacecraft)
act.function = functools.partial(func, "Gyro5FineMode")
spacecraft.add_activity(act)

act = Activity("InsertIntoSchedule", spacecraft)
act.function = functools.partial(func, "InsertIntoSchedule")
spacecraft.add_activity(act)


# reporting data

rep = ReportingData("ManoeuvreEndTime")
rep.function = lambda x: ureg('-95 s')
spacecraft.add_reporting_data(rep)

rep = ReportingData("CrossTrackError")
rep.function = lambda x: ureg('10 arcmin')
spacecraft.add_reporting_data(rep)
