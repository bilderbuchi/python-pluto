from pluto import *


class Procedure_test1051(Procedure):

    def main(self):
        stp = Step_HeatingUp(self)
        if self.initiate_and_confirm_step(stp) is False:
            return False


class Step_HeatingUp(Step):

    def declare(self):
        self.variable['Status'] = Variable(bool)
        self.variable['AvgeTemp'] = Variable(float, "degC")

    def main(self):
        self.assignment('Status', self.value_expression("ValidityStatus of Pump of FreonLoop1"))
        arguments = [
            {
                "StartTime": Argument(self.value_expression("CurrentTime() - 10 min")),
                "EndTime": Argument(self.value_expression("CurrentTime()"))
            }
        ]
        self.assignment('AvgeTemp', self.value_expression("average value of FreonTemp", arguments=arguments))
