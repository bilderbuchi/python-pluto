from pluto import *


class Procedure_test0541(Procedure):

	def declare(self):
		wdg = Watchdog_ManoeuvreDampingFailed(self, "Cross-track error not reducing to less than 5 arcmin within 100 s of manoeuvre completion")
		self.watchdog['ManoeuvreDampingFailed'] = wdg

	def main(self):
		stp = Step_ManoeuvreDampingFailureRoutine(self)
		if self.initiate_and_confirm_step(stp) is False:
			return False

class Step_ManoeuvreDampingFailureRoutine(Step):

	def main(self):
		timeout = get_model(self).get_reporting_data('ManoeuvreEndTime').get_value() + ureg("100 s")
		raise_event = 'ManoeuvreDampingFailed'
		if self.wait_until_condition("CrossTrackError", "<", ureg("5 arcmin"), timeout=timeout, raise_event=raise_event) is False:
			return False


class Watchdog_ManoeuvreDampingFailed(Watchdog):

    def main(self):
        self.log("...")
