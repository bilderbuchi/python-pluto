from pluto import *


class Procedure_test0921(Procedure):

    def preconditions(self):
        if self.wait_until_condition("GyroTemp > 60 degC") is False:
            return False

    def main(self):
        act = Activity(self, "SwitchOnGyroConverter")
        if self.initiate_and_confirm_activity(act) is False:
            return False
        act = Activity(self, "SwitchOnGyro5")
        if self.initiate_and_confirm_activity(act) is False:
            return False
        act = Activity(self, "Gyro5FineMode")
        if self.initiate_and_confirm_activity(act) is False:
            return False

    def confirmation(self):
        if self.wait_until_condition("Output of Gryo5 < 0.2 deg/h") is False:
            return False
