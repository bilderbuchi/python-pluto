from pluto import *


class Procedure_test0521(Procedure):

    def main(self):
        stp = Step_EclipseEntryRoutine(self)
        if self.initiate_and_confirm_step(stp) is False:
            return False


class Step_EclipseEntryRoutine(Step):

    def main(self):
        if self.wait_for_event("EclipseEntry", save_context=["Temperature of Battery1 by TempBatt1", "Voltage of Battery1 by VoltBatt1"]) is False:
            return False
