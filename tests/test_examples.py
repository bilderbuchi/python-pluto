from pathlib import Path
import os

import pytest

from pluto import parse
from pluto.utils import pluto_to_python, pluto_run


EXAMPLES_FOLDER = 'examples'
EXAMPLE_DIR = Path(__file__).parent.parent / EXAMPLES_FOLDER
EXAMPLES = sorted([e.relative_to(EXAMPLE_DIR) for e in EXAMPLE_DIR.glob('*.pluto')])


@pytest.mark.parametrize('example', EXAMPLES, ids=str)
def test_examples_parse(example):
    """The available pluto examples parse successfully.

    Note that this does not mean that the parse or source is correct."""
    with open(EXAMPLE_DIR / (str(example))) as f:
        contents = f.read()
    print('PLUTO script:')
    print(contents)
    tree = parse(contents)


# @pytest.mark.skip(reason="transformer incomplete, activate this test once script conversion has been enabled.")
@pytest.mark.parametrize('example', EXAMPLES, ids=str)
def test_examples_generate_reference(example):
    """The available pluto examples and Python reference are in sync."""
    pluto_file = Path(EXAMPLE_DIR / example.with_suffix('.pluto'))
    py_file = Path(EXAMPLE_DIR / example.with_suffix('.py'))
    if not py_file.exists():
        pytest.skip('Reference for example {} not found.'.format(example))
    with open(pluto_file) as f:
        pluto_script = f.read()
    with open(py_file) as f:
        py_script = f.read()
    generated_script, tree = pluto_to_python(
        pluto_script, procedure_name=str(example.stem), debug=True)
    print(tree.pretty())
    assert generated_script == py_script


@pytest.mark.skip(reason="some examples require input and thus break the test")
@pytest.mark.parametrize('example', EXAMPLES)
def test_examples_run_converted_scripts(example):
    py_file = Path(EXAMPLE_DIR / (example.stem + '.py'))
    if not py_file.exists():
        pytest.skip('Reference for example {} not found.'.format(example))
    pluto_run(os.path.join(EXAMPLES_FOLDER, py_file.name))
