import pathlib
import sys
import os
import time
import importlib
import logging

from .parser import PlutoParser
from .transformer import PlutoTransformer
from pluto.language.constants import COMPLETED, CONFIRMED, ABORTED


def pluto_to_python(pluto_string, procedure_name='anon', debug=False):
    """Convert a string containing a PLUTO procedure into Python source.

    Having this as a separate method enables doing this without
    touching the file system.

    The procedure_name is needed to set the name of the generated
    Python class.
    If debug is True, return the transformed output and the tree.
    """
    tree = PlutoParser.parse(pluto_string)
    transformer = PlutoTransformer(procedure_name=procedure_name)
    transformed = transformer.transform(tree)
    if not debug:
        return transformed
    else:
        return transformed, tree


def pluto_convert(pluto_file=None):
    """Convert a .pluto file into a Python file at the same location.

    The file location can be passed as an argument to the function or
    as an argument to the registered console_script entrypoint.
    """
    if pluto_file is None:
        if len(sys.argv) != 2:
            raise Exception("Please provide .pluto file as an argument")
        pluto_file = sys.argv[1]

    with open(pluto_file) as f:
        contents = f.read()
    pluto_file_path = pathlib.Path(pluto_file)
    proc_name = pluto_file_path.stem
    python_source = pluto_to_python(contents, procedure_name=proc_name)
    py_filename = pluto_file_path.with_suffix('.py')
    with open(py_filename, 'w') as py:
        py.write(python_source)


def pluto_run(file=None):
    """Run a .py file that was converted form a Pluto script.

    The file location can be passed as an argument to the function or
    as an argument to the registered console_script entrypoint.
    """
    if file is None:
        if len(sys.argv) != 2:
            raise Exception("Please provide .py file as an argument")
        file = sys.argv[1]

    path = file.split('.')[0]  # only keep name of file
    folder, filename = os.path.split(path)

    if folder:
        module_path = ".".join([folder, filename])
    else:
        module_path = filename

    sys.path.append(os.path.abspath(folder))
    proc_module = importlib.import_module(module_path)
    Procedure = getattr(proc_module, "Procedure_" + filename)
    procedure = Procedure()

    # set to INFO for log statements only,
    # and to DEBUG to include debug information
    logging.basicConfig(level=logging.DEBUG)

    print("Running procedure [Procedure_{}]".format(filename))
    print(80 * "-")
    procedure.run()
    while procedure.get_execution_status() != COMPLETED:
        time.sleep(0)
    print(80 * "-")
    if procedure.get_confirmation_status() == CONFIRMED:
        print("Procedure completed successfully.")
    elif procedure.get_confirmation_status() == ABORTED:
        print("Procedure was aborted!")
    else:
        print("Procedure confirmation error!")
