"""Transformers that turn a parsed tree of PLUTO code into Python source.

IMPLEMENTATION NOTES:
* Every rule in the grammar needs a method in the respective transformer.
* In the docstring of the method, note the relevant section of the standard
  defining the logic. The PLUTO standard (ECSS‐E‐ST‐70‐32C, shorthand: E32) is
  assumed by default, if referencing ECSS-E-ST-70-31C, prefix E31.
* In implementation code, where appropriate note the relevant section in the
  standard (e.g. A.3.9.34-Definition)
* If the implementation remains incomplete or otherwise not adhering to the
  standard, leave a "TODO:" or "FIXME:" comment.
* Skeleton implementations are there. Modify the list of parameters. You can
  use Lark's `@va_inline` decorator if wanted.
* Hint: It's probably easiest to start from the leaves of the tree and work
  towards the root.
* The method needs to be tested. Add tests in `test_[Pluto|Unit]Transformer.py`
  as appropriate. Name rule tests as `test_<rulename>_<some_postfix>`.
* You can use `pluto_parser` test fixture, which accepts a `start` kwarg. This
  way, you can create a tree that starts at the rule under question, which
  simplifies the PLUTO code you have to feed to the parser to test. See
  the already existing tests.
* Test all configurations, edge cases, and also erroneous examples ( using
  e.g. `pytest.raises`) if possible.
"""
from collections import OrderedDict
from textwrap import indent

import pint
from lark import Transformer, v_args, Token


# Python source snippets
PREAMBLE = """\
from pluto import *
from model import *
"""

ureg = pint._DEFAULT_REGISTRY
# Add PLUTO-specific units as defined in section B.2
ureg.define('Ohm = ohm')
ureg.define('r = revolution')
ureg.define('AU = au')
# TODO: Remove the following as soon as pint PR #811 is released
# https://github.com/hgrecco/pint/pull/811
ureg.define('d = day')  # waiting for pint #811 to be released
ureg.define('neper = [] = Np')  # waiting for pint #811 to be released
ureg.autoconvert_offset_to_baseunit = True  # workaround for #22


class UnitsTransformer(Transformer):
    """Transformer for the engineering units grammar."""
    def engineering_units(self, args):
        """Parse and return unit strings."""
        return ''.join(args)

    def unit_reference(self, args):
        return ''.join(args)

    def unit_product(self, args):
        return ''.join(args)

    def unit_factor(self, args):
        return ''.join(args)

    def unit_simple_factor(self, args):
        return ''.join(args)

    def unit_exponent(self, args):
        return ''.join(args)


class PlutoTransformer(UnitsTransformer):
    """Transformer for the PLUTO grammar.

    The rule implementations return strings fit for assembling a Python script.
    Methods and attributes which are not Rules implementations are prefixed
    with _ to avoid name collisions.
    """

    def __init__(self, procedure_name='anon'):
        """Constructor to make the UnitsTransformer methods available."""
        # FIXME: This is hacky, did not see another way to achieve this in Lark
        # Make sure the namespaced rules from the engineering units grammar get
        # picked up
        for k in UnitsTransformer.__dict__.keys():
            if not k.startswith('__'):
                setattr(self, 'engineering_units__' + k, getattr(self, k))
        self._procedure_name = procedure_name

        # Initialize the generated python source string
        self._root_items = OrderedDict()
        self._root_items['preamble'] = str(PREAMBLE)
        self._root_items['procedure'] = ''  # a placeholder for correct order

    # FIXME: Bug in ECSS grammar: string constant and enumerated constant are actually identical!
    # FIXME: Not clear how to handle strings (include quotes?) and how to tell Lark that quotes and backslash in CHARACTERS has to be escaped

    def _indented(self, addition, level=0):
        """Conveniently indent string by an indentation level."""
        return indent(addition, ' ' * level * 4)

    def activity_call(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        return Token('activity_call', args[0])

    def activity_reference(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        return Token('activity_reference', args[0])

    def argument_reference(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        return Token('argument_reference', args[0])

    def arguments(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def array_argument(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    @v_args(inline=True)
    def assignment_statement(self, var_ref, expression):
        """Store the result of an expression in the appropriate scope."""
        # FIXME: Allowed scope for stored variables as per the standard needs to be implemented
        raise NotImplementedError

    @v_args(inline=True)
    def boolean_constant(self, cons):
        # TODO: Convert back to terminal as soon as Lark can transform terminals (#389)
        res = 'True' if cons.value == 'TRUE' else 'False'
        return Token('boolean_constant', res)

    @v_args(inline=True)
    def boolean_operator(self, op):
        # TODO: Convert back to terminal as soon as Lark can transform terminals (#389)
        # Use Python's bitwise operators
        return Token('boolean_operator', 'operator.{}_'.format(op.value.lower()))

    def case_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def case_tag(self):
        """See section XXX
        """
        # Note: first clarify precedence rules of chained boolean expressions
        # see also https://docs.python.org/3/reference/expressions.html#operator-precedence
        # TODO: Incomplete
        raise NotImplementedError

    def comparative_expression(self, args):
        """See section A.3.9.34
        """
        # TODO: Incomplete
        res = ''.join(args)
        return Token('comparative_expression', res)

    def confirmation_body(self, stmts):
        """See section A.3.9.32
        """
        source = 'def confirmation(self):\n'
        for stmt in stmts:
            source += self._indented(stmt, 1)
        return Token('confirmation_body', source)

    def constant(self, args):
        """See section A.4.2"""
        # TODO: Incomplete'
        if args[0].type == 'ABSOLUTE_TIME_CONSTANT':
            time_cons = args[0]
            date, time = time_cons[0:-1].split('T')
            year, month, day = date.split('-')
            hour, minute, sec = time.split(':')
            second, microsec = sec.split('.')
            res = 'datetime({}, {}, {}, {}, {}, {}, {})'.format(
                int(year), int(month), int(day),
                int(hour), int(minute), int(second), int(microsec))
        else:
            res = ''.join(args)
        return Token('constant', res)

    def continuation_action(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def continuation_test(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def description(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def directives(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def enumerated_set_declaration(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def enumerated_set_reference(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def event_declaration(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        event_name = args[0]
        event_declaration = self._indented(
            'class Event_' + event_name + '(Event):\n\n'
        )
        event_declaration += '    pass\n'
        self._root_items[event_name] = event_declaration
        res = 'evt = Event_{}(self)\n'.format(args[0])
        res += 'self.event[\'{}\'] = evt\n'.format(args[0])
        return Token('event_declaration', res)

    def event_reference(self, args):
        """See section XXX
        """
        return Token('event_reference', args[0])

    def expression(self, args):
        """See section A.3.9.34.
        """
        res = ''.join(args)
        return Token('expression', res)

    def factor(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        if args[0].type == 'simple_factor':
            res = str(args[0])
        else:
            raise NotImplementedError
        return Token('factor', res)

    def flow_control_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def for_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def function(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def if_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def inform_user_statement(self, args):
        """See section XXX
        """
        parts = []
        for arg in args:
            parts.append("str(" + str(arg) + ")")
        res = " + ".join(parts)
        return Token(
            'inform_user_statement',
            "self.inform_user({})\n".format(res))

    def initiate_activity_statement(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        activity_name = args[0]
        res = "act = ActivityCall(self, {})\n".format(activity_name)
        res += "self.initiate_activity(act)\n".format(activity_name)
        return Token('initiate_activity_statement', res)

    def initiate_and_confirm_activity_statement(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        activity_name = args[0]
        res = "act = ActivityCall(self, {})\n".format(activity_name)
        res += "if self.initiate_and_confirm_activity(act) is False: return False\n".format(activity_name)
        return Token('initiate_and_confirm_activity_statement', res)

    def initiate_and_confirm_step_statement(self, args):
        """See section A.3.9.12"""
        # TODO: Incomplete
        step_name = args[0]
        step_definition = self._indented(
            'class Step_' + step_name + '(Step):\n\n'
        )
        step_definition += self._indented(args[1], 1)  # append step definition
        # TODO: continuation test treatment missing here
        self._root_items[step_name] = step_definition
        res = "stp = Step_{}(self)\n".format(step_name)
        res += "if self.initiate_and_confirm_step(stp) is False: return False\n".format(step_name)
        return Token('initiate_and_confirm_step_statement', res)

    def initiate_and_confirm_step_statement_watchdog(self, args):
        """See section A.3.9.12"""
        # TODO: Incomplete
        step_name = args[0]
        step_definition = self._indented(
            'class Watchdog_' + step_name + '(Watchdog):\n\n'
        )
        step_definition += self._indented('\n'.join(args[1:]), 1)  # append step definition
        # TODO: continuation test treatment missing here
        self._root_items[step_name] = step_definition
        res = "wdg = Watchdog_{}(self)\n".format(step_name)
        res += "self.watchdog['{}'] = wdg\n".format(step_name)
        return Token('initiate_and_confirm_step_statement_watchdog', res)

    def initiate_in_parallel_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def integer_constant(self, args):
        """Parse integer constant into a string defining a Pint quantity.

        Use pint string parsing:
        https://pint.readthedocs.io/en/0.9/tutorial.html#string-parsing
        """
        # TODO: Incomplete
        if args[0].type == 'HEXADECIMAL_CONSTANT':
            res = "int('{}', 16)".format(str(args[0]))
        else:
            res = "ureg('{}')".format(''.join(args))
        return Token('integer_constant', res)

    def log_statement(self, args):
        parts = []
        for arg in args:
            parts.append("str(" + str(arg) + ")")
        res = " + ".join(parts)
        return Token('log_statement', "self.log({})\n".format(res))

    def object_operation(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def object_operation_request_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def object_property(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def object_property_request(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def object_reference(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        sys_elems = []
        for sys_elem in reversed(args):
            if sys_elem != 'activity':
                sys_elems.append(sys_elem)
        res = '.'.join(sys_elems)
        return Token('object_reference', res)

    def preconditions_body(self, stmts):
        """See section XXX
        """
        # TODO: Incomplete
        source = 'def preconditions(self):\n'
        for stmt in stmts:
            source += self._indented(stmt, 1)
        return Token('preconditions_body', source)

    def predefined_type(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def predefined_value_set_reference(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def procedure_declaration_body(self, ev_dcls):
        """See section XXX
        """
        source = 'def declare(self):\n'
        for ev_dcl in ev_dcls:
            source += self._indented(ev_dcl, 1)
        return Token('procedure_declaration_body', source)

    def procedure_definition(self, bodies):
        """Create and return the procedure text."""
        proc = self._indented(
            'class Procedure_' + self._procedure_name + '(Procedure):\n\n'
        )
        proc += self._indented('\n'.join(bodies), 1)  # append all bodies in order
        self._root_items['procedure'] = proc

        full_source = '\n\n'.join(self._root_items.values())
        return Token('procedure_definition', full_source)

    def procedure_main_body(self, proc_stmts):
        """Process all procedure statements encountered."""
        source = 'def main(self):\n'
        for stmt in proc_stmts:
            source += self._indented(stmt, 1)
        return Token('procedure_main_body', source)

    def procedure_statement(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        return Token('procedure_statement', args[0])

    def product(self, args):
        """See section A.3.9.34"""
        # TODO: Incomplete
        res = ''.join(args)
        return Token('product', res)

    def property_data_type(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def property_value_set(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def raise_event(self, args):
        """See section XXX
        """
        res = ' raise_event=\'{}\''.format(args[0])
        return Token('raise_event', res)

    def real_constant(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        res = ''.join(args)
        return Token('real_constant', "ureg('{}')".format(res))

    def record_argument(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def relational_expression(self, args):
        """See section A.3.9.34
        """
        res = ''.join(args)
        return Token('relational_expression', res)

    def relative_time_constant(self, args):
        """See section A.3.6.f.3
        """
        # TODO: Incomplete
        # TODO: Find out if either ureg or timedelta can parse all the possible combinations - tests!
        # Attention, pint interprets "h" as Planck's constant, not hour!
        # FIXME: This will only work for simple cases. Maybe detour via datetime and convert to ureg?
        return Token('relative_time_constant', "ureg('{}')".format(''.join(args)))

    def repeat_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def reporting_data_reference(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def save_context(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def save_context_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    @v_args(inline=True)
    def set_procedure_context_statement(self, obj_ref, *procedure_stmts):
        """See section XXX
        """
        raise NotImplementedError

    def set_step_context_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def step_declaration_body(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def step_definition(self, bodies):
        """See section A.3.9.13"""
        # TODO: Incomplete?
        source = self._indented('\n'.join(bodies))  # append all bodies in order
        return Token('step_definition', source)

    def simple_argument(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def simple_factor(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        if args[0].type == 'constant':
            res = str(args[0])
        elif args[0].type == 'SIGN':
            res = ''.join(args)
        elif args[0].type == 'variable_reference':
            res = str(args[0])
        else:
            raise NotImplementedError
        return Token('simple_factor', res)

    def step_main_body(self, stmts):
        """See section A.3.9.15"""
        source = 'def main(self):\n'
        for stmt in stmts:
            source += self._indented(stmt, 1)
        return Token('step_main_body', source)

    @v_args(inline=True)
    def step_statement(self, stmt):
        """See section A.3.9.15"""
        return Token('step_statement', stmt)

    def system_element_reference(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def term(self, args):
        """See section A.3.9.34"""
        # TODO: Incomplete
        res = ''.join(args)
        return Token('term', res)

    def timeout(self, args):
        """See section XXX
        """
        res = ' timeout='
        res += ','.join(args)
        return Token('timeout', res)

    def variable_declaration(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError

    def variable_reference(self, args):
        """See section XXX
        """
        # TODO: Incomplete
        return Token('variable_reference', args[0])

    def wait_statement(self, args):
        return Token("wait_statement", args[0])

    def wait_for_statement(self, args):
        """See section A.3.9.4
        The wait for statement applies to relative times.
        """
        return Token(
            "wait_for_statement",
            "self.wait_for_relative_time({})\n".format(args[0]))

    def wait_for_event_statement(self, args):
        """See section A.3.9.4
        The wait for event statement applies to events.
        """
        event_name = args[0]
        return Token(
            "wait_for_event_statement",
            "if self.wait_for_event('{}') is False: return False\n".format(event_name))

    def wait_until_statement(self, args):
        """See section A.3.9.4
        The wait until statement applies to absolute times and
        boolean expressions.
        """
        # Workaround to handle wait for absolute time
        if 'datetime(' in args[0]:
            return Token(
                "wait_for_statement",
                "self.wait_until_absolute_time({})\n".format(args[0]))
        else:
            return Token(
                "wait_for_statement",
                "if self.wait_until_expression(lambda: {}) is False: return False\n".format(','.join(args)))

    def watchdog_body(self, stmts):
        """See section XXX
        """
        # TODO: Incomplete
        source = 'def register_watchdogs(self):\n'
        for stmt in stmts:
            source += self._indented(stmt, 1)
        return Token('watchdog_body', source)

    def while_statement(self):
        """See section XXX
        """
        # TODO: Incomplete
        raise NotImplementedError
