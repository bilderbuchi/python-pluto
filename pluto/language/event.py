

class Event:

    def __init__(self, parent, description=None):
        self.parent = parent
        self.description = description
        self.triggered = False

    def has_triggered(self):
        return self.triggered

    def trigger(self):
        self.triggered = True

    def reset(self):
        self.triggered = False
