from .constants import INVALID, VALID


class DataContainer:

    def __init__(self, datatype=None, value=None, units=None, description=None):
        self.datatype = datatype
        self.value = value
        self.units = units
        self.description = None
        self.validity_status = INVALID

    """Operation requests"""

    def set_validity_status(self, status):
        self.validity_status = status

    def set_value(self, value):
        self.value = value
        self.validity_status = VALID

    """Property requests"""

    def get_value(self):
        return self.value

    def get_average_value(self, start, end):
        raise NotImplementedError


class Variable(DataContainer):
    pass


class Argument(DataContainer):
    pass
