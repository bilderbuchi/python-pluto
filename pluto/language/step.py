from .constants import NOT_INITIATED, NOT_AVAILABLE
from .procedure import Procedure, atomic_statement


class Step(Procedure):

    def __init__(self, parent):
        self.parent = parent

        self.execution_status = NOT_INITIATED
        self.confirmation_status = NOT_AVAILABLE
        self.variable = {}
        self.continuation = {}
        self.watchdog = {}
        self.event = {}
        self.abort_flag = False
        self.active_watchdogs = 0

    """Main Body Statements"""

    @atomic_statement
    def set_step_context(self, context):
        raise NotImplementedError

    @atomic_statement
    def save_context(self, context):
        raise NotImplementedError

    @atomic_statement
    def assignment(self, variable_name, expression):
        variable = self.variable[variable_name]
        expression = variable.datatype(expression)
        variable.set_value(expression)

    """Property Requests"""

    def get_initiation_time(self):
        raise NotImplementedError

    def get_start_time(self):
        raise NotImplementedError

    def get_termination_time(self):
        raise NotImplementedError

    def get_restart_number(self):
        raise NotImplementedError

    def get_completion_time(self):
        raise NotImplementedError
