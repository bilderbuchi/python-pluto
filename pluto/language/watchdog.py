import threading

from .constants import NOT_INITIATED, PRECONDITIONS, EXECUTING, CONFIRMATION
from .constants import NOT_AVAILABLE
from .step import Step


class Watchdog(Step):

    def __init__(self, parent, description=None):
        self.parent = parent
        self.description = description

        self.execution_status = NOT_INITIATED
        self.confirmation_status = NOT_AVAILABLE
        self.event = {}
        self.abort_flag = False
        self.active_watchdogs = 0

    def run(self):
        self.execution_status = NOT_INITIATED
        self.confirmation_status = NOT_AVAILABLE

        self.execution_status = PRECONDITIONS
        if self.preconditions() is False:
            return self.abort()

        self.execution_status = EXECUTING
        self.parent.active_watchdogs += 1
        main_thread = threading.Thread(target=self.main)
        main_thread.start()
        main_thread.join()
        if self.parent.active_watchdogs > 0:
            self.parent.active_watchdogs -= 1

        if self.abort_flag is True:
            self.parent.abort_flag = True
            return self.abort()

        self.execution_status = CONFIRMATION
        if self.confirmation() is False:
            return self.not_confirm()
        else:
            return self.confirm()
