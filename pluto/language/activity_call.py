from .step import Step
from .constants import NOT_INITIATED, PRECONDITIONS, ROUTING, EXECUTING,\
    CONFIRMATION
from .constants import NOT_AVAILABLE, NOT_CONFIRMED


class ActivityCall(Step):
    """Run an external (model-provided) activity, eg. sending an email."""

    def __init__(self, parent, activity_ref, arguments=None):
        self.parent = parent
        self.activity_ref = activity_ref
        self.arguments = arguments

        self.execution_status = NOT_INITIATED
        self.confirmation_status = NOT_AVAILABLE

        self.continuation = {}
        self.watchdog = None  # keep as None, as activity has no watchdog

    def run(self):
        self.execution_status = NOT_INITIATED
        self.confirmation_status = NOT_AVAILABLE

        # preconditions
        self.execution_status = PRECONDITIONS

        # routing
        self.execution_status = ROUTING

        # executing
        self.execution_status = EXECUTING
        if self.activity_ref.run(self.arguments) is False:
            return self.abort()

        # confirmation
        self.execution_status = CONFIRMATION
        if self.activity_ref.get_confirmation_status() == NOT_CONFIRMED:
            return self.not_confirm()
        else:
            return self.confirm()
