import threading
import time
import logging
from functools import wraps

from .constants import NOT_INITIATED, PRECONDITIONS, EXECUTING,\
    CONFIRMATION, COMPLETED
from .constants import NOT_AVAILABLE, NOT_CONFIRMED, CONFIRMED, ABORTED
from .constants import CONTINUE, ASK_USER, RESTART, ABORT, RAISE_EVENT
from .functions import current_time


SLEEP_TIME = 0.5


def get_watchdog(object, watchdog_name):
    if object.watchdog is None or object.watchdog.get(watchdog_name) is None:
        if object.parent is None:
            return None
        else:
            return get_watchdog(object.parent, watchdog_name)
    else:
        return object.watchdog.get(watchdog_name)


def get_event(object, event_name):
    if object.event is None or object.event.get(event_name) is None:
        if object.parent is None:
            return None
        else:
            return get_event(object.parent, event_name)
    else:
        return object.event.get(event_name)


def atomic_statement(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        while self.active_watchdogs > 0:
            time.sleep(SLEEP_TIME)
        if self.abort_flag:
            return False
        return func(self, *args, **kwargs)
    return wrapper


class Procedure:

    def __init__(self, model=None, parent=None):
        self.model = model
        self.parent = parent

        self.execution_status = NOT_INITIATED
        self.confirmation_status = NOT_AVAILABLE
        self.watchdog = {}
        self.event = {}
        self.abort_flag = False
        self.active_watchdogs = 0

    def run(self):
        self.execution_status = NOT_INITIATED
        self.confirmation_status = NOT_AVAILABLE
        self.declare()

        self.execution_status = PRECONDITIONS
        if self.preconditions() is False:
            return self.abort()

        self.execution_status = EXECUTING
        self.register_watchdogs()
        for watchdog in self.watchdog:
            wdg_thread = threading.Thread(target=self.watchdog[watchdog].run)
            wdg_thread.start()

        main_thread = threading.Thread(target=self.main)
        main_thread.start()
        main_thread.join()

        # abort all active watchdogs
        for watchdog in self.watchdog:
            self.watchdog[watchdog].abort_flag = True

        if self.abort_flag is True:
            return self.abort()

        self.execution_status = CONFIRMATION
        if self.confirmation() is False:
            return self.not_confirm()
        else:
            return self.confirm()

    def proceed(self):
        while self.active_watchdogs > 0:
            time.sleep(SLEEP_TIME)
        if self.abort_flag:
            return False

    def abort(self):
        self.execution_status = COMPLETED
        self.confirmation_status = ABORTED
        return

    def not_confirm(self):
        self.execution_status = COMPLETED
        self.confirmation_status = NOT_CONFIRMED
        return

    def confirm(self):
        self.execution_status = COMPLETED
        self.confirmation_status = CONFIRMED
        return

    """Body Elements"""

    def register_watchdogs(self):
        pass

    def declare(self):
        """"Local events that can be raised within the procedure, accessible
        to all child steps. Define here the global watchdogs.
        """
        pass

    def preconditions(self):
        """Contains the conditions that define wether a procedure can start.
        To fail preconditions, return False.
        """
        pass

    def main(self):
        """A sequence of statements."""
        pass

    def confirmation(self):
        """Confirms whether procedure has achieved its objective.
        To fail confirmation, return False
        """
        pass

    """Main Body Statements"""

    @atomic_statement
    def set_procedure_context(self):
        raise NotImplementedError

    @atomic_statement
    def initiate_in_parallel(self, *steps):
        raise NotImplementedError
        # for step in steps:
        #     thread = threading.Thread(target=step.run)
        #     thread.start()
        # TODO: wait for at least one to complete

    @atomic_statement
    def initiate_in_parallel_until_all_complete(self, *steps):
        raise NotImplementedError
        # threads = []
        # for step in steps:
        #     thread = threading.Thread(target=step.run)
        #     threads.add(thread)
        #     thread.start()
        # for thread in threads:
        #     thread.join()

    def continuation_test(self, step, confirmation_status):
        if confirmation_status == CONFIRMED:
            logging.debug("step/activity confirmed")
            continuation_action = step.continuation.get(CONFIRMED)
            if continuation_action is None:
                continuation_action = CONTINUE

        elif confirmation_status == NOT_CONFIRMED:
            logging.debug("step/activity not confirmed")
            continuation_action = step.continuation.get(NOT_CONFIRMED)
            if continuation_action is None:
                continuation_action = ASK_USER

        elif confirmation_status == ABORTED:
            logging.debug("step/activity aborted")
            continuation_action = step.continuation.get(ABORTED)
            if continuation_action is None:
                continuation_action = ABORT
        else:
            raise ValueError

        return self.do_continuation(step, continuation_action)

    def do_continuation(self, step, continuation_action):
        if continuation_action == ABORT:
            self.abort_flag = True
            return False
        elif continuation_action == RESTART:
            return self.initiate_and_confirm_step(step)

        elif continuation_action == ASK_USER:
            while True:
                answer = self.ask_user(
                    "->Not successful. (A)bort, (R)estart, (C)ontinue? ")
                answer = answer.upper()
                if answer in ['A', 'R', 'C']:
                    break
            if answer == "A":
                logging.debug("abort...")
                self.abort_flag = True
                return False
            elif answer == "R":
                logging.debug("restart...")
                return self.initiate_and_confirm_step(step)
            elif answer == "C":
                logging.debug("continue...")
                return

        elif continuation_action == RAISE_EVENT:
            raise NotImplementedError
            # watchdog = get_watchdog(step, step.raise_event)
            # watchdog.run()
            # if watchdog.confirmation_status == ABORTED:
            #     return self.abort()  # abort procedure
            # if watchdog.confirmation_status == NOT_CONFIRMED:
            #     return False  # terminate procedure
            # return self.confirm()  # resume procedure

        elif continuation_action == CONTINUE:
            return

        else:
            raise ValueError

    @atomic_statement
    def initiate_and_confirm_step(self, step):
        step.run()
        while step.execution_status != COMPLETED:
            time.sleep(SLEEP_TIME)  # wait for completion
        return self.continuation_test(step, step.get_confirmation_status())

    @atomic_statement
    def initiate_and_confirm_activity(self, activity):
        return self.initiate_and_confirm_step(activity)

    @atomic_statement
    def initiate_activity(self, activity):
        thread = threading.Thread(target=activity.run)
        thread.start()

    @atomic_statement
    def inform_user(self, message):
        logging.info(message)

    @atomic_statement
    def log(self, message):
        logging.info(message)

    """Property Requests"""

    def get_execution_status(self):
        return self.execution_status

    def get_confirmation_status(self):
        return self.confirmation_status

    def set_confirmation_status(self, status):
        self.confirmation_status = status

    """Other Statements"""

    @atomic_statement
    def wait_until_absolute_time(self, absolute_time):
        while current_time() < absolute_time:
            time.sleep(SLEEP_TIME)

    @atomic_statement
    def wait_for_relative_time(self, relative_time):
        time.sleep(relative_time.to('s').magnitude)

    @atomic_statement
    def wait_for_event(self, event_ref, save_context=None, timeout=None):
        event = get_event(self, event_ref)
        if event is None:
            raise ValueError
        while not event.has_triggered() and not self.abort_flag:
            time.sleep(SLEEP_TIME)
        if self.abort_flag:
            return False

    @atomic_statement
    def wait_until_expression(
            self, expression,
            save_context=None, timeout=None, raise_event=None):

        if timeout is None:
            while self.expression(expression) is False and not self.abort_flag:
                time.sleep(SLEEP_TIME)
            if self.abort_flag:
                return False
            else:
                return  # finished

        # timeout is provided
        end_time = current_time() + timeout
        while current_time() < end_time and not self.abort_flag:
            if self.expression(expression):
                return  # finished
            time.sleep(SLEEP_TIME)
        if self.abort_flag:
            return False

        # timeout reached
        if raise_event is None:
            return False
        event = get_event(self, raise_event)
        event.trigger()
        time.sleep(SLEEP_TIME)

    @atomic_statement
    def expression(self, expression):
        return expression()

    @atomic_statement
    def ask_user(self, question):
        answer = input(question)
        return answer
