
# Execution status
NOT_INITIATED = "not initiated"
PRECONDITIONS = "preconditions"
ROUTING = "routing"
EXECUTING = "executing"
CONFIRMATION = "confirmation"
COMPLETED = "completed"


# Confirmation status
NOT_AVAILABLE = "not available"
CONFIRMED = "confirmed"
NOT_CONFIRMED = "not confirmed"
ABORTED = "aborted"


# Continuation action
ABORT = "abort"
RESTART = "restart"
ASK_USER = "ask user"
RAISE_EVENT = "raise event"
CONTINUE = "continue"


# Validity status
INVALID = "invalid"
VALID = "valid"
